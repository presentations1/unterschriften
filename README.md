Warum ist ein eingescannter und in ein elektronisches Dokument
eingefügter Namenszug ein Bild und keine
[Unterschrift](https://de.wikipedia.org/wiki/Unterschrift)?

Ganz einfach: Dem Bild fehlen die wesentlichen Merkmale einer
Signatur. Diese Präsentation soll zeigen, welche das sind und wie man
ein elektronisches Dokument trotzdem signieren kann.
